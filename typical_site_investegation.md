Typical Site Investigation Processes & Items
=====================================================


## Site works

- Cable Percussion Boreholes
- SPT Tests
- Samples
- Insitu CBR tests
- Trial pits
- Samples
- Hand Vane Tests
- Ground Water Monitoring

## Lab Testing

- Water Content Test
- Atterberg Limits Test
- Compaction Tests
- Particle Size Distribution Test
- Triaxial Test
- Consolidation Test

## Site Investigation Report

- Main text
- Exploratory Hole Location Plan
- Laboratory Test Sheets
- Exploratory Hole Log Sheets
- Monitoring Sheets
- AGS Data

## Documents

- Lab test schedule
- Chain of Custody Form
- Test Amendment Notice
- Specification